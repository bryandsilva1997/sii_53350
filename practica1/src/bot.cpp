#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMCompartida.h"
#include "Esfera.h"
//#include "Raqueta.h"
int main()
{
	int f;
	DatosMCompartida* pMComp;
	char *proyeMemoria;
	f=open("/tmp/datosBot.txt",O_RDWR);
	if(f==-1){
		printf("Error al abrir el Bot\n");
		exit(1);
	}
	proyeMemoria=(char*)mmap(NULL,sizeof(*(pMComp)),PROT_WRITE|PROT_READ,MAP_SHARED,f,0);
	close(f);
	pMComp=(DatosMCompartida*)proyeMemoria;
	while(1)
	{
		usleep(10000);
		float posRaqueta1;
		posRaqueta1=((pMComp->raqueta1.y2+pMComp->raqueta1.y1)/2);
		if(posRaqueta1<pMComp->esfera.centro.y)
			pMComp->accion=1;
		else if(posRaqueta1>pMComp->esfera.centro.y)
			pMComp->accion=-1;
		else 
			pMComp->accion=0;
	}
	munmap(proyeMemoria,sizeof(*(pMComp)));
	return 0;
}
