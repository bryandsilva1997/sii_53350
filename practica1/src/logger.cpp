
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
int main (int argc, char *argv[])
{
	mkfifo("/tmp/MITUBERIA",0777);
	int fd=open("/tmp/MITUBERIA",O_RDONLY);
	if(fd<0){
		perror("Fallo al abrir el fifo");
		return(1);
	}
	int aux;
	char buff[200];
	while(1)
	{
		aux=read(fd,buff,sizeof(buff));
		if(aux<0)
		{	
			perror("fallo lectura");
			return(1);
		}
		if(aux==0)
		{	
			perror("fin de programa");
			return(1);
		}
		printf("%s\n",buff);
	}
	close(fd);
	unlink("/tmp/MITUBERIA");
	return 0;
}
